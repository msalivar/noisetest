﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMesh : MonoBehaviour
{
    MeshFilter filter;
    MeshCollider coll;
    public MeshData data;

    public void RenderMesh()
    {
        filter = gameObject.GetComponent<MeshFilter>();
        coll = gameObject.GetComponent<MeshCollider>();
        filter.mesh.Clear();
        filter.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        filter.mesh.vertices = data.vertices.ToArray();
        filter.mesh.triangles = data.triangles.ToArray();
        filter.mesh.uv = data.uv.ToArray();
        filter.mesh.RecalculateNormals();
    }
}