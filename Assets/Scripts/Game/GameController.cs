﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private TileGeneration map;
       
    [SerializeField]
    private GameObject creaturePrefab;

    private List<Creep> creeps = new List<Creep>();

    public float timeSinceLastRun = 0f;
    public float lerpDuration = 0.25f;

    private void Start()
    {
        SpawnCreature(0, 10);
        InvokeRepeating("RunCreepBehaviors", 2f, 0.5f);
    }

    void Update()
    {
        if (timeSinceLastRun < lerpDuration)
        {
            timeSinceLastRun += Time.deltaTime;
        }
    }

    private void RunCreepBehaviors()
    {
        timeSinceLastRun = 0f;
        foreach (var creep in creeps)
        {
            creep.RunBehavior();
        }
    }

    private void SpawnCreature(int x, int z)
    {
        var location = GetMapVector(x, z);
        var creep = Instantiate(creaturePrefab, location, Quaternion.identity);
        var creepObject = creep.GetComponentInChildren<Creep>();
        creepObject.gameController = this;
        creepObject.CurrentMapCoordinates = (x, z);
        creepObject.TargetPosition = location;
        creeps.Add(creepObject);
    }

    public Vector3 GetMapVector(int x, int z)
    {
        return new Vector3(
            x * map.cubeDimensions + (map.cubeDimensions / 2), 
            map.gameHeightMap[x,z] + map.cubeDimensions / 4,
            z * map.cubeDimensions + (map.cubeDimensions / 2));
    }
}
