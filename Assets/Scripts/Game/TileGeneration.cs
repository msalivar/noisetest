﻿using UnityEngine;

public class TileGeneration : MonoBehaviour
{
    #region Private Serialized Fields

    [SerializeField]
    NoiseMapGeneration noiseMapGeneration;

    [SerializeField]
    private int cubeScale;

    [SerializeField]
    private int mapDimensions;

    [SerializeField]
    private float noiseScale;

    [SerializeField]
    private float heightMultiplier;

    [SerializeField]
    private int waterHeight;

    [SerializeField]
    private int rockDepth;

    [SerializeField]
    private AnimationCurve heightCurve;

    [SerializeField]
    private bool RandomizeSeeds;

    [SerializeField]
    public Wave[] waves;

    [SerializeField]
    private GameObject meshPrefab;

    #endregion

    #region Private Fields

    private Cube[,,] cubes;

    #endregion

    #region Public Fields

    public int numCubes;
    public float cubeDimensions;
    public float[,] gameHeightMap;

    #endregion

    private void Awake()
    {
        if(RandomizeSeeds)
        {
            for(int i = 0; i < waves.Length; i++)
            {
                waves[i].seed = Random.Range(1f, 10000f);
            }
        }

        GenerateTile();
    }

    private void GenerateTile()
    {
        numCubes = mapDimensions * cubeScale;
        cubeDimensions = (float)mapDimensions / (float)numCubes;

        float[,] heightMap = noiseMapGeneration.GenerateNoiseMap(numCubes, numCubes, noiseScale, waves);
        gameHeightMap = new float[numCubes, numCubes];

        cubes = new Cube[numCubes, numCubes, numCubes];

        for(int z = 0; z < numCubes; z++)
        {
            for(int x = 0; x < numCubes; x++)
            {
                int height = Mathf.RoundToInt(heightMap[z, x] * heightMultiplier);
                gameHeightMap[x, z] = (float)height * cubeDimensions;

                for(int y = 0; y < numCubes; y++)
                {
                    if(y < height)
                    {
                        cubes[x, y, z] = new CubeGrass(cubeDimensions);
                    }
                    else
                    {
                        cubes[x, y, z] = new CubeAir(cubeDimensions);
                    }
                }
            }
        }

        // Water
        int waterLevel = Mathf.RoundToInt(GetMinimumHeight(heightMap) * heightMultiplier);
        if(waterLevel + waterHeight < numCubes)
        {
            waterLevel += waterHeight;
            for(int z = 0; z < numCubes; z++)
            {
                for(int x = 0; x < numCubes; x++)
                {
                    if(!cubes[x, waterLevel + 1, z].IsSolid(Cube.Direction.up))
                    {
                        SpawnWater(x, waterLevel, z);
                    }
                }
            }
        }
        else
        {
            Debug.Log("Skipped spawning water.");
        }

        //Rocks
        int rockLevel = Mathf.RoundToInt(GetMaximumHeight(heightMap) * heightMultiplier);
        if(rockLevel + rockDepth < numCubes)
        {
            rockLevel -= rockDepth;
            for(int z = 0; z < numCubes; z++)
            {
                for(int x = 0; x < numCubes; x++)
                {
                    if(cubes[x, rockLevel, z].IsSolid(Cube.Direction.up))
                    {
                        SpawnRock(x, rockLevel, z);
                    }
                }
            }
        }
        else
        {
            Debug.Log("Skipped spawning rocks.");
        }

        UpdateChunk();
    }

    private void SpawnWater(int x, int y, int z)
    {
        cubes[x, y, z] = new CubeWater(cubeDimensions);
        
        if(y > 0 && !cubes[x, y - 1, z].IsSolid(Cube.Direction.down))
        {
            SpawnWater(x, y - 1, z);
        }
    }

    private void SpawnRock(int x, int y, int z)
    {
        cubes[x, y, z] = new Cube(cubeDimensions);

        if(y < numCubes - 1 && cubes[x, y + 1, z].IsSolid(Cube.Direction.up))
        {
            SpawnRock(x, y + 1, z);
        }
    }

    private float GetMinimumHeight(float[,] heightMap)
    {
        float min = mapDimensions * cubeScale;

        for(int x = 0; x < heightMap.GetLength(0); x++)
        {
            for(int y = 0; y < heightMap.GetLength(1); y++)
            {
                if(heightMap[x, y] < min)
                {
                    min = heightMap[x, y];
                }
            }
        }

        return min;
    }

    private float GetMaximumHeight(float[,] heightMap)
    {
        float max = 0;

        for(int x = 0; x < heightMap.GetLength(0); x++)
        {
            for(int y = 0; y < heightMap.GetLength(1); y++)
            {
                if(heightMap[x, y] > max)
                {
                    max = heightMap[x, y];
                }
            }
        }

        return max;
    }

    private void UpdateChunk()
    {
        MeshData meshData = new MeshData();
        int cubeCount = 0;

        for(int x = 0; x < numCubes; x++)
        {
            for(int y = 0; y < numCubes; y++)
            {
                for(int z = 0; z < numCubes; z++)
                {
                    meshData = cubes[x, y, z].Blockdata(this, x, y, z, numCubes, meshData);
                    cubeCount++;

                    if(cubeCount > 30000)
                    {
                        SpawnMesh(meshData);
                        meshData = new MeshData();
                        cubeCount = 0;
                    }
                }
            }
        }

        SpawnMesh(meshData);
    }

    private void SpawnMesh(MeshData meshData)
    {
        GameObject mapMesh = Instantiate(meshPrefab, transform.position, Quaternion.identity, transform) as GameObject;
        var script = mapMesh.GetComponent<MapMesh>();
        script.data = meshData;
        script.RenderMesh();
    }

    public Cube GetCube(int x, int y, int z)
    {
        return cubes[x, y, z];
    }
}
