﻿public interface IBehavior
{
    void Run(Creep creep);
}