﻿using System.Collections.Generic;

public class GeneralBehavior : IBehavior
{
    private Creep thisCreep;

    public GeneralBehavior(Creep creep)
    {
        thisCreep = creep;
    }

    public void Run(Creep creep)
    {
        switch(creep.state)
        {
            case BehaviorStates.Moving:
                RunMoving();
                break;
            case BehaviorStates.Idle:
                RunIdle();
                break;
            default:
                break;
        }
    }

    protected virtual void RunIdle()
    {
        // decided to move
        BeginMoveState();
    }

    protected virtual void RunMoving()
    {
        thisCreep.state = BehaviorStates.Idle;
    }

    protected virtual void RunGathering()
    {
    }

    protected virtual void RunEating()
    {
    }

    protected virtual void BeginMoveState()
    {
        thisCreep.SetTargetPosition(1, 0);
        thisCreep.state = BehaviorStates.Moving;
    }
}
