﻿public enum BehaviorStates
{
    Idle,
    Moving,
    Gathering,
    Eating
}