﻿using UnityEngine;

public class Creep : MonoBehaviour
{
    private IBehavior behavior;
    private int healthPoints = 100;
    private int hungerLevel = 100;
    private bool animation_started;
    private bool animation_finished;

    public GameController gameController;
    public (int, int) CurrentMapCoordinates;
    public Vector3 CurrentPosition { get; set; }
    public Vector3 TargetPosition { get; set; }
    public BehaviorStates state { get; set; }

    public Creep()
    {
        behavior = new GeneralBehavior(this);
        state = BehaviorStates.Idle;
    }

    void Update()
    {
        switch (state)
        {
            case BehaviorStates.Moving:
                transform.position = Vector3.Lerp(transform.position, TargetPosition,
                    gameController.timeSinceLastRun / gameController.lerpDuration);
                break;
        }
    }

    public void RunBehavior()
    {
        behavior.Run(this);
    }

    public void LateUpdate()
    {
        
    }

    public void SetTargetPosition(int x, int z)
    {
        CurrentMapCoordinates = (CurrentMapCoordinates.Item1 + x, CurrentMapCoordinates.Item2 + z);
        TargetPosition = gameController.GetMapVector(CurrentMapCoordinates.Item1, CurrentMapCoordinates.Item2);
    }
}
