﻿using UnityEngine;

public class CubeWater : Cube
{
    public CubeWater(float size = 0.5f) : base(size)
    {
    }

    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        tile.x = 0;
        tile.y = 1;

        return tile;
    }
}