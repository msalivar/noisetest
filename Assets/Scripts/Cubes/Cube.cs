﻿using UnityEngine;

public class Cube
{
    public enum Direction { north, east, south, west, up, down };

    public struct Tile
    {
        public int x;
        public int y;
    }

    private float cubeSize;

    const float tileSize = 0.25f;

    public Cube(float size = 0.5f)
    {
        cubeSize = size;
    }

    public virtual MeshData Blockdata(TileGeneration tile, int x, int y, int z, int dimensions, MeshData meshData)
    {
        if(y == dimensions - 1 || !tile.GetCube(x, y + 1, z).IsSolid(Direction.down))
        {
            meshData = FaceDataUp(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        if(y == 0 || !tile.GetCube(x, y - 1, z).IsSolid(Direction.up))
        {
            meshData = FaceDataDown(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        if(z == dimensions - 1 || !tile.GetCube(x, y, z + 1).IsSolid(Direction.south))
        {
            meshData = FaceDataNorth(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        if(z == 0 || !tile.GetCube(x, y, z - 1).IsSolid(Direction.north))
        {
            meshData = FaceDataSouth(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        if(x == dimensions - 1 || !tile.GetCube(x + 1, y, z).IsSolid(Direction.west))
        {
            meshData = FaceDataEast(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        if(x == 0 || !tile.GetCube(x - 1, y, z).IsSolid(Direction.east))
        {
            meshData = FaceDataWest(x * cubeSize, y * cubeSize, z * cubeSize, meshData);
        }

        return meshData;
    }

    protected virtual MeshData FaceDataUp(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z));
        meshData.AddVertex(new Vector3(x, y + cubeSize, z));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.up));
        return meshData;
    }

    protected virtual MeshData FaceDataDown(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x, y, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y, z + cubeSize));
        meshData.AddVertex(new Vector3(x, y, z + cubeSize));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.down));
        return meshData;
    }

    protected virtual MeshData FaceDataNorth(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x + cubeSize, y, z + cubeSize));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x, y, z + cubeSize));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.north));
        return meshData;
    }

    protected virtual MeshData FaceDataEast(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x + cubeSize, y, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x + cubeSize, y, z + cubeSize));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.east));
        return meshData;
    }

    protected virtual MeshData FaceDataSouth(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x, y, z));
        meshData.AddVertex(new Vector3(x, y + cubeSize, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y + cubeSize, z));
        meshData.AddVertex(new Vector3(x + cubeSize, y, z));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.south));
        return meshData;
    }

    protected virtual MeshData FaceDataWest(float x, float y, float z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x, y, z + cubeSize));
        meshData.AddVertex(new Vector3(x, y + cubeSize, z + cubeSize));
        meshData.AddVertex(new Vector3(x, y + cubeSize, z));
        meshData.AddVertex(new Vector3(x, y, z));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.west));
        return meshData;
    }

    //protected virtual MeshData FaceDataUp(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize + cubeSize, z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize + cubeSize, z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize + cubeSize, z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize + cubeSize, z * cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.up));
    //    return meshData;
    //}

    //protected virtual MeshData FaceDataDown(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize, z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize, z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize, z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize, z * cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.down));
    //    return meshData;
    //}

    //protected virtual MeshData FaceDataNorth(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize, y * cubeSize, z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize + cubeSize,    z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize + cubeSize,    z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize,               z * cubeSize + cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.north));
    //    return meshData;
    //}

    //protected virtual MeshData FaceDataSouth(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize,               z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize,             y * cubeSize + cubeSize,    z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize + cubeSize,    z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize,  y * cubeSize,               z * cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.south));
    //    return meshData;
    //}

    //protected virtual MeshData FaceDataEast(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize, y * cubeSize + cubeSize,    z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize, y * cubeSize + cubeSize,    z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize, y * cubeSize,               z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize + cubeSize, y * cubeSize,               z * cubeSize + cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.east));
    //    return meshData;
    //}

    //protected virtual MeshData FaceDataWest(int x, int y, int z, MeshData meshData)
    //{
    //    meshData.vertices.Add(new Vector3(x * cubeSize, y * cubeSize + cubeSize,    z * cubeSize + cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize, y * cubeSize + cubeSize,    z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize, y * cubeSize,               z * cubeSize));
    //    meshData.vertices.Add(new Vector3(x * cubeSize, y * cubeSize,               z * cubeSize + cubeSize));

    //    meshData.AddQuadTriangles();
    //    meshData.uv.AddRange(FaceUVs(Direction.west));
    //    return meshData;
    //}

    public virtual bool IsSolid(Direction direction)
    {
        switch(direction)
        {
            case Direction.north:
                return true;
            case Direction.east:
                return true;
            case Direction.south:
                return true;
            case Direction.west:
                return true;
            case Direction.up:
                return true;
            case Direction.down:
                return true;
        }

        return false;
    }

    public virtual Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();
        tile.x = 0;
        tile.y = 0;

        return tile;
    }

    public virtual Vector2[] FaceUVs(Direction direction)
    {
        Vector2[] UVs = new Vector2[4];
        Tile tilePos = TexturePosition(direction);

        UVs[0] = new Vector2(tileSize * tilePos.x + tileSize,
            tileSize * tilePos.y);
        UVs[1] = new Vector2(tileSize * tilePos.x + tileSize,
            tileSize * tilePos.y + tileSize);
        UVs[2] = new Vector2(tileSize * tilePos.x,
            tileSize * tilePos.y + tileSize);
        UVs[3] = new Vector2(tileSize * tilePos.x,
            tileSize * tilePos.y);

        return UVs;
    }
}