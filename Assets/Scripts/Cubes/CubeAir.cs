﻿using UnityEngine;
using System.Collections;

public class CubeAir : Cube
{
    public CubeAir(float size = 0.5f) : base(size)
    {
    }

    public override MeshData Blockdata(TileGeneration tile, int x, int y, int z, int dimensions, MeshData meshData)
    {
        return meshData;
    }

    public override bool IsSolid(Direction direction)
    {
        return false;
    }
}